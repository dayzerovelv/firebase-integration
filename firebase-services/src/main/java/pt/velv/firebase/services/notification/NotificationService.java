package pt.velv.firebase.services.notification;

import pt.velv.firebase.model.notification.NotificationRequest;
import pt.velv.firebase.model.response.FirebaseResponse;

public interface NotificationService {

	FirebaseResponse sendNotification(final NotificationRequest notificationRequest);
}
