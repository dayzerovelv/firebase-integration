package pt.velv.firebase.services.notification.log;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import pt.velv.firebase.model.notification.NotificationLog;
import pt.velv.firebase.model.notification.NotificationRequest;
import pt.velv.firebase.model.response.FirebaseResponse;
import pt.velv.firebase.model.response.paged.PagedNotificationLog;
import pt.velv.firebase.repository.notification.log.NotificationLogRepository;
import pt.velv.firebase.services.notification.NotificationServiceImpl;

@Service

public class NotificationSearchServiceImpl implements NotificationSearchService {

    private static final Log LOG = LogFactory.getLog(NotificationServiceImpl.class);

    @Autowired
    private NotificationLogRepository repository;


    @Override
    public void logNotification(final NotificationRequest request, final FirebaseResponse response) {
        this.repository.insert(from(request, response.toString()));
    }

    @Override
    public void setLogNotificationAsReadById(String id) {
        Optional<NotificationLog> optional = repository.findById(id);
        if (optional.isPresent()) {
            NotificationLog logToUpdate = optional.get();
            logToUpdate.setRead(true);
            this.repository.save(logToUpdate);
            return;
        }

        LOG.warn("UPDATE NO NOTIFICATION FOUND FOR ID " + id);
    }

    @Override
    public void setLogNotificationAsRead(NotificationRequest request) {
        NotificationLog logToUpdate = findByNotificationTypeAndEntityId(request.getNotificationType(), request.getEntityId());
        if (logToUpdate != null) {
            logToUpdate.setRead(true);
            this.repository.save(logToUpdate);
            return;
        }

        LOG.warn("UPDATE NO NOTIFICATION FOUND FOR ENTITY ID " + request.getEntityId() + " AND TYPE " + request.getNotificationType());
    }


    @Override
    public List<NotificationLog> findReceivedNotificationsByUserId(final String userId) {
        return repository.findByToUserId(userId);
    }

    private NotificationLog from(final NotificationRequest notification, final String firebaseResponse) {
        final NotificationLog result = new NotificationLog();

        result.setCreatedDate(Instant.now());
        result.setEntityId(notification.getEntityId());
        result.setFromUserId(notification.getFromUserId());
        result.setToUserId(notification.getToUserId());
        result.setNotificationToken(notification.getNotificationToken());
        result.setNotificationType(notification.getNotificationType());
        result.setSentMessage(notification.getMessageToSend());
        result.setCustomType(notification.getCustomType());
        result.setCustomImage(notification.getCustomImage());
        result.setFirebaseResponse(firebaseResponse);
        result.setRead(notification.isRead());

        return result;
    }

    @Override
    public PagedNotificationLog findByFromUserIdAndNotificationTypeIn(final String userId, final List<String> notificationTypes, final int page, final int limit) {
        final Pageable pageable = PageRequest.of(page, limit, new Sort(Sort.Direction.DESC, "createdDate"));
        final Page<NotificationLog> pageLog = repository.findByFromUserIdAndNotificationTypeIn(userId, notificationTypes, pageable);

        final PagedNotificationLog result = new PagedNotificationLog();
        result.setTotalElements(pageLog.getTotalElements());
        result.setTotalPages(pageLog.getTotalPages());
        result.setNotifications(pageLog.getContent());

        return result;
    }

    @Override
    public PagedNotificationLog findByToUserIdAndNotificationTypeIn(final String userId, final List<String> notificationTypes, final int page, final int limit) {
        final Pageable pageable = PageRequest.of(page, limit, new Sort(Sort.Direction.DESC, "createdDate"));
        final Page<NotificationLog> pageLog = repository.findByToUserIdAndNotificationTypeIn(userId, notificationTypes, pageable);

        final PagedNotificationLog result = new PagedNotificationLog();
        result.setTotalElements(pageLog.getTotalElements());
        result.setTotalPages(pageLog.getTotalPages());
        result.setNotifications(pageLog.getContent());

        return result;
    }

    @Override
    public NotificationLog findByNotificationTypeAndEntityId(String notificationType, String entityId) {
        Optional<NotificationLog> optional = repository.findByNotificationTypeAndEntityId(notificationType, entityId);
        if (optional.isPresent()) {
            return optional.get();
        }
        return null;
    }
}
