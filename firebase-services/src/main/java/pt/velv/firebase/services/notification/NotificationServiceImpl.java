package pt.velv.firebase.services.notification;

import java.nio.charset.Charset;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import pt.velv.firebase.model.notification.NotificationRequest;
import pt.velv.firebase.model.response.FirebaseResponse;
import pt.velv.firebase.services.notification.log.NotificationSearchService;

@Service
@PropertySource("classpath:firebase.properties")
public class NotificationServiceImpl implements NotificationService {

	private static final Log LOG = LogFactory.getLog(NotificationServiceImpl.class);

	@Value("${secretKey}")
	private String secretKey;

    @Value("${fcmUrl}")
	private String fcmUrl;

    @Autowired
    private NotificationSearchService notificationLogService;

	@Override
	public FirebaseResponse sendNotification(final NotificationRequest notificationToSend) {
		if(StringUtils.isNotBlank(notificationToSend.getNotificationToken())) {
			final FirebaseResponse response = sendFirebaseNotification(notificationToSend);
			this.notificationLogService.logNotification(notificationToSend, response);
			return response;
		} 

		LOG.warn("User " + notificationToSend.getToUserId() + " does not have a token to send a notification");
		final FirebaseResponse noToken = FirebaseResponse.noToken();
		this.notificationLogService.logNotification(notificationToSend, noToken);
		return noToken;
	}
	
	private FirebaseResponse sendFirebaseNotification(final NotificationRequest notificationRequest) {
		final String title = notificationRequest.getTitle();
		final String messageToSend = notificationRequest.getMessageToSend();
		final String notificationToken = notificationRequest.getNotificationToken();
		
		final String customType = notificationRequest.getCustomType();
		final String customImage = notificationRequest.getCustomImage();
		
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Authorization", "key=" + secretKey);
        httpHeaders.set("Content-Type", "application/json");

        final JSONObject notification = new JSONObject();
        notification.put("title", title);
        notification.put("body", messageToSend);
        notification.put("sound", "default");

        final JSONObject json = new JSONObject();
        
        if(StringUtils.isNotBlank(customType)) {
        	final JSONObject data = new JSONObject();
			data.put("custom_type", customType);
			
			if(StringUtils.isNotBlank(customImage)) {
				data.put("custom_image", customImage);
			}

			json.put("data", data);
		}
        
        json.put("notification", notification);

        json.put("to", notificationToken);

        final HttpEntity<String> httpEntity = new HttpEntity<String>(json.toString(), httpHeaders);
        final RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter((Charset.forName("UTF-8"))));
        final String response = restTemplate.postForObject(fcmUrl, httpEntity, String.class);

        LOG.info("Firebase response: " + response);

        final Gson gson = new Gson();
        return gson.fromJson(response, FirebaseResponse.class);
    }

}
