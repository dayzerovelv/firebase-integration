package pt.velv.firebase.services.notification.log;

import java.util.List;

import pt.velv.firebase.model.notification.NotificationLog;
import pt.velv.firebase.model.notification.NotificationRequest;
import pt.velv.firebase.model.response.FirebaseResponse;
import pt.velv.firebase.model.response.paged.PagedNotificationLog;

public interface NotificationSearchService {

    void logNotification(NotificationRequest request, FirebaseResponse response);
    
    List<NotificationLog> findReceivedNotificationsByUserId(String userId);
    
    PagedNotificationLog findByFromUserIdAndNotificationTypeIn(String userId, List<String> notificationTypes, int page, int limit);
    
    PagedNotificationLog findByToUserIdAndNotificationTypeIn(String userId, List<String> notificationTypes, int page, int limit);

    NotificationLog findByNotificationTypeAndEntityId(String notificationType, String entityId);

    void setLogNotificationAsReadById(String id);

    void setLogNotificationAsRead(NotificationRequest request);
}
