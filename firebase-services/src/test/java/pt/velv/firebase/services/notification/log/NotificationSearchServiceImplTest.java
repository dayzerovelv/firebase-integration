package pt.velv.firebase.services.notification.log;


import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

import pt.velv.firebase.model.notification.NotificationRequest;
import pt.velv.firebase.model.response.FirebaseResponse;
import pt.velv.firebase.repository.notification.log.NotificationLogRepository;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:spring-firebase-services.xml"})
public class NotificationSearchServiceImplTest {
	
	@Autowired
	private NotificationSearchService searchService;
	
	@Autowired
	private NotificationLogRepository repository;
	
	
	private final String user1 = "user1";
	private final String user2 = "user2";
	private final String user3 = "user3";
	private final String[] notificationTypes = {"nType1", "nType2", "nType3"};


	@Before
	public void setUp() throws Exception {
		final FirebaseResponse response = new FirebaseResponse();

		final NotificationRequest request1 = build(user1, user2, notificationTypes[0]); 
		searchService.logNotification(request1, response);
		
		final NotificationRequest request2 = build(user1, user3, notificationTypes[1]);
		searchService.logNotification(request2, response);
		
		final NotificationRequest request3 = build(user2, user3, notificationTypes[0]);
		searchService.logNotification(request3, response);
		
		final NotificationRequest request4 = build(user2, user1, notificationTypes[1]);
		searchService.logNotification(request4, response);
		
		final NotificationRequest request5 = build(user3, user1, notificationTypes[2]);
		searchService.logNotification(request5, response);
		
		final NotificationRequest request6 = build(user3, user2, notificationTypes[2]);
		searchService.logNotification(request6, response);
	}

	
	@After
	public void tearDown() throws Exception {
		repository.deleteAll();
	}

	
//	@Test
//	public void testFindNotificationsByTypeAndUserIdQuery() {
//		// given
//		final List<String> notificationTypesList = Arrays.asList(notificationTypes[1]);
//		
//		// when
//		final List<NotificationLog> notifications = searchService.findNotificationsByTypeAndUserId(user1, notificationTypesList, 0, 10);
//		
//		// then
//		Assert.assertNotNull(notifications);
//		
//		Assert.assertEquals(2, notifications.size());
//	}
//	
//	@Test
//	public void testFindNotificationsByTypeAndUserIdPage() {
//		// given
//		final List<String> notificationTypesList = Arrays.asList(notificationTypes[1]);
//		
//		// when
//		final List<NotificationLog> notifications = searchService.findNotificationsByTypeAndUserId(user1, notificationTypesList, 0, 1);
//		
//		// then
//		Assert.assertNotNull(notifications);
//		
//		Assert.assertEquals(1, notifications.size());
//	}
//	
//	@Test
//	public void testFindNotificationsByTypeAndUserIdAllNotificationTypes() {
//		// given
//		final List<String> notificationTypesList = Arrays.asList(notificationTypes);
//		
//		// when
//		final List<NotificationLog> notifications = searchService.findNotificationsByTypeAndUserId(user1, notificationTypesList, 0, 10);
//		
//		// then
//		Assert.assertNotNull(notifications);
//		
//		Assert.assertEquals(4, notifications.size());
//	}
	
	private NotificationRequest build(final String fromUserId, final String toUserId, final String notificationType) {
		final NotificationRequest request = new NotificationRequest();
		
		request.setFromUserId(fromUserId);
		request.setToUserId(toUserId);
		request.setNotificationType(notificationType);
		
		return request;
	}
}
