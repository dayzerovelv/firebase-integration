package pt.velv.firebase.repository.notification.log;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import pt.velv.firebase.model.notification.NotificationLog;

public interface NotificationLogRepository extends MongoRepository<NotificationLog, String> {

	List<NotificationLog> findByFromUserId(String userId);

	List<NotificationLog> findByToUserId(String userId);
	
	Page<NotificationLog> findByFromUserIdAndNotificationTypeIn(String userId, List<String> notificationTypes, Pageable page);
	
	Page<NotificationLog> findByToUserIdAndNotificationTypeIn(String userId, List<String> notificationTypes, Pageable page);

	Optional<NotificationLog> findByNotificationTypeAndEntityId(String notificationType, String entityId);
}
