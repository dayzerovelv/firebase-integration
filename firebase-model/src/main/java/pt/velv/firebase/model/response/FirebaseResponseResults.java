package pt.velv.firebase.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FirebaseResponseResults implements Serializable {

	private static final long serialVersionUID = 365299255520025961L;

	@JsonProperty("message_ids")
	private String messageId;


	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(final String messageId) {
		this.messageId = messageId;
	}
}
