package pt.velv.firebase.model.notification;

public interface NotificationType {

	String getNotificationType();
}
