package pt.velv.firebase.model.response.paged;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pt.velv.firebase.model.notification.NotificationLog;

public class PagedNotificationLog implements Serializable {

	private static final long serialVersionUID = -8861251129357541297L;

	private Integer totalPages;
    private Long totalElements;
    
    List<NotificationLog> notifications = new ArrayList<>();

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(final Integer totalPages) {
		this.totalPages = totalPages;
	}

	public Long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(final Long totalElements) {
		this.totalElements = totalElements;
	}

	public List<NotificationLog> getNotifications() {
		return notifications;
	}

	public void setNotifications(final List<NotificationLog> notifications) {
		this.notifications = notifications;
	}
}
