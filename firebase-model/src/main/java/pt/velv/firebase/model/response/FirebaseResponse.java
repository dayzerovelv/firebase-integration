package pt.velv.firebase.model.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;

public class FirebaseResponse implements Serializable {

	private static final long serialVersionUID = 3595786480257741094L;

	@JsonProperty("multicast_id")
	private String multicastId;

	@JsonProperty("canonical_ids")
	private long canonicalIds;

	private int success;
	private int failure;
	private List<FirebaseResponseResults> results;
	
	public static FirebaseResponse noToken() {
		final FirebaseResponse response = new FirebaseResponse();
		
		response.setMulticastId("NO_USER_TOKEN");
		response.setFailure(1);
		response.setSuccess(0);
		
		return response;
	}

	public String getMulticastId() {
		return multicastId;
	}

	public void setMulticastId(final String multicastId) {
		this.multicastId = multicastId;
	}

	public long getCanonicalIds() {
		return canonicalIds;
	}

	public void setCanonicalIds(final long canonicalIds) {
		this.canonicalIds = canonicalIds;
	}

	public boolean isSuccess() {
		return success == 1;
	}

	public void setSuccess(final int success) {
		this.success = success;
	}

	public int getSuccess() {
		return success;
	}

	public int getFailure() {
		return failure;
	}

	public boolean isFailure() {
		return failure == 1;
	}

	public void setFailure(final int failure) {
		this.failure = failure;
	}

	public List<FirebaseResponseResults> getResults() {
		return results;
	}

	public void setResults(final List<FirebaseResponseResults> results) {
		this.results = results;
	}

	@Override
	public String toString() {
		final Gson gson = new Gson();
		return gson.toJson(this);
	}
}
