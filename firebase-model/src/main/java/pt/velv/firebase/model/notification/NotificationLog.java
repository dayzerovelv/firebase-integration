package pt.velv.firebase.model.notification;

import java.io.Serializable;
import java.time.Instant;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity
public class NotificationLog implements Serializable {

	private static final long serialVersionUID = 3489987482560573791L;

	@Id
	private String id;

	private Instant createdDate;
	private String notificationType;
	private String entityId;
	private String sentMessage;
	private String fromUserId;
	private String toUserId;
	private String notificationToken;
	private String customType;
	private String customImage;
	private String firebaseResponse;
	private boolean read = false;


	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(final Instant createdDate) {
		this.createdDate = createdDate;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(final String notificationType) {
		this.notificationType = notificationType;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(final String entityId) {
		this.entityId = entityId;
	}

	public String getSentMessage() {
		return sentMessage;
	}

	public void setSentMessage(final String sentMessage) {
		this.sentMessage = sentMessage;
	}

	public String getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(final String fromUserId) {
		this.fromUserId = fromUserId;
	}

	public String getToUserId() {
		return toUserId;
	}

	public void setToUserId(final String toUserId) {
		this.toUserId = toUserId;
	}

	public String getNotificationToken() {
		return notificationToken;
	}

	public void setNotificationToken(final String notificationToken) {
		this.notificationToken = notificationToken;
	}

	public String getCustomType() {
		return customType;
	}

	public void setCustomType(final String customType) {
		this.customType = customType;
	}

	public String getCustomImage() {
		return customImage;
	}

	public void setCustomImage(final String customImage) {
		this.customImage = customImage;
	}

	public String getFirebaseResponse() {
		return firebaseResponse;
	}

	public void setFirebaseResponse(final String firebaseResponse) {
		this.firebaseResponse = firebaseResponse;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}
}
