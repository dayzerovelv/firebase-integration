package pt.velv.firebase.model.notification;

import java.io.Serializable;

public class NotificationRequest implements Serializable {

	private static final long serialVersionUID = -55286428255042429L;

	private String notificationType;
	private String entityId;
	private String messageToSend;
	private String title;
	private String fromUserId;
	private String toUserId;
	private String notificationToken;
	private String customType;
	private String customImage;
	private boolean read;


	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(final String notificationType) {
		this.notificationType = notificationType;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(final String entityId) {
		this.entityId = entityId;
	}

	public String getMessageToSend() {
		return messageToSend;
	}

	public void setMessageToSend(final String messageToSend) {
		this.messageToSend = messageToSend;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(final String fromUserId) {
		this.fromUserId = fromUserId;
	}

	public String getToUserId() {
		return toUserId;
	}

	public void setToUserId(final String toUserId) {
		this.toUserId = toUserId;
	}

	public String getNotificationToken() {
		return notificationToken;
	}

	public void setNotificationToken(final String notificationToken) {
		this.notificationToken = notificationToken;
	}

	public String getCustomType() {
		return customType;
	}

	public void setCustomType(final String customType) {
		this.customType = customType;
	}

	public String getCustomImage() {
		return customImage;
	}

	public void setCustomImage(final String customImage) {
		this.customImage = customImage;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}
}
